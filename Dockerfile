FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/projectInter-0.0.2.jar
WORKDIR /opt/app
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","app.jar"]