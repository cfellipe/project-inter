# Project inter

##### Rodando a aplicação

- Clone o repositório na sua pasta de preferência:
- Utilize o maven para criar o artifactory do projeto com o seguinte comando no terminal.
   > **mvn clean package** 
- Utilize o seguinte comando [Docker](https://docs.docker.com/get-docker/) para criar a imagem do projeto no terminal.
  > **docker build -t projectinter &nbsp; .**
- Utilize o seguinte comando do [Docker Compose](https://docs.docker.com/compose/) para executar o projeto no terminal.
  > **docker-compose up**

##### Swagger

Com a aplicação executada, acesse o [Swagger](http://localhost:3001/swagger-ui.html) que possui documentação dos endpoints criados, ou importe a coleção do postman existente no projeto.

##### Rodando os testes da aplicação

- Utilize o maven para executar os testes da aplicação com o seguinte comando no terminal.
  > **mvn test** 


##### Importante
De maneira opcional foi adicionado autenticação no endpoint para [cálculo de dígitos](localhost:3001/digits) e no endpoint [busca de digitos por usuário ](localhost:3001/digits/user).
Para utilizar os recursos deste endpoints é necessário [criar um usuário](localhost:3001/user), e consequentemente realizar a [autenticação](localhost:3001/auth) para a geração do token necessário.


