package com.inter.projectInter.repository;


import com.inter.projectInter.entity.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    private User user;

    @After
    public void tearDown(){
        userRepository.deleteAll();
    }

    @Before
    public void setUp(){
        user = userRepository.save(buildUser());
    }

    @Test
    public void shouldFindUser() {
        assertTrue(userRepository.findById(user.getId()).isPresent());
    }

    @Test
    public void shouldDeleteUser() {
        assertEquals(1, userRepository.findAll().size());
        userRepository.delete(user);
        assertEquals(0, userRepository.findAll().size());
    }

    @Test
    public void shouldUpdateUser() {
        User foundUser = userRepository.findById(user.getId()).get();
        assertEquals("inter", foundUser.getName());
        assertEquals("project@inter.com", foundUser.getEmail());
        foundUser.setName("projectInter");
        foundUser.setEmail("project@inter.com.br");
        userRepository.save(foundUser);
        foundUser = userRepository.findById(user.getId()).get();
        assertEquals("projectInter", foundUser.getName());
        assertEquals("project@inter.com.br", foundUser.getEmail());
    }


    public static User buildUser() {
        return new User("inter", "project@inter.com");
    }
}
