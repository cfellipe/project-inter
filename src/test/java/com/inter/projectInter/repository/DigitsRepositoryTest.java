package com.inter.projectInter.repository;


import com.inter.projectInter.entity.Digits;
import com.inter.projectInter.entity.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DigitsRepositoryTest {

    @Autowired
    private DigitsRepository digitsRepository;

    @Autowired
    private UserRepository userRepository;

    @After
    public void tearDown(){
        digitsRepository.deleteAll();
    }

    @Test
    public void shouldSaveNewDigit() {
        digitsRepository.save(buildDigit());
        assertEquals(1, digitsRepository.findAll().size());
    }

    @Test
    public void shoudGetDigitsByUser(){
        Digits digits = buildDigit();
        User user = UserRepositoryTest.buildUser();
        digits.setUser(user);
        userRepository.save(user);
        digitsRepository.save(digits);
        digits = buildDigit();
        digits.setUser(user);
        digitsRepository.save(digits);
        assertEquals(2,digitsRepository.findDigitsByUser(user).size());

    }


    private Digits buildDigit() {
        return new Digits("123", 3);
    }
}
