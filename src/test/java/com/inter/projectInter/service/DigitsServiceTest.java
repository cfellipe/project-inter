package com.inter.projectInter.service;


import com.inter.projectInter.dto.DigitsDTO;
import com.inter.projectInter.entity.Digits;
import com.inter.projectInter.entity.User;
import com.inter.projectInter.repository.DigitsRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DigitsServiceTest {

    @MockBean
    private DigitsRepository digitsRepository;

    @Autowired
    private DigitsService digitsService;

    private User user;

    @Before
    public void setUp() {
        user = new User("Inter", "project@inter.com");
        given(digitsRepository.save(Mockito.any(Digits.class))).willReturn(new Digits());
    }

    @After
    public void tearDown() {
        digitsService.setCacheDigitis(new ArrayList<>(10));
    }

    @Test
    public void shouldCalculateDigits() {
        assertEquals(Integer.valueOf(12), digitsService.calculate(new DigitsDTO("123", 2), user));
        assertEquals(Integer.valueOf(6), digitsService.calculate(new DigitsDTO("123", 0), user));
        assertEquals(Integer.valueOf(116), digitsService.calculate(new DigitsDTO("9875", 4), user));
    }

    @Test
    public void shouldTestCache() {
        digitsService.calculate(new DigitsDTO("123", 2), user);
        digitsService.calculate(new DigitsDTO("123", 0), user);
        digitsService.calculate(new DigitsDTO("9875", 4), user);
        digitsService.calculate(new DigitsDTO("123", 2), user);
        digitsService.calculate(new DigitsDTO("123", 0), user);
        digitsService.calculate(new DigitsDTO("9875", 4), user);
        verify(digitsRepository, times(3)).save(Mockito.any(Digits.class));
        assertEquals(3, digitsService.getCacheDigitis().size());
    }

    @Test
    public void shouldGetTestByUser(){
        digitsService.calculate(new DigitsDTO("123", 2), user);
        digitsService.calculate(new DigitsDTO("123", 0), user);

        given(digitsRepository.findDigitsByUser(Mockito.any(User.class))).willReturn(Arrays.asList(new Digits("2",2), new Digits("123",0)));
        List<DigitsDTO> digitsByUser = digitsService.getDigitsByUser(user);
        assertEquals(2,digitsByUser.size());

    }
}
