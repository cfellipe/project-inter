package com.inter.projectInter.service;


import com.inter.projectInter.config.exception.DataIntegrityViolationException;
import com.inter.projectInter.dto.DigitsDTO;
import com.inter.projectInter.dto.UserRequestDTO;
import com.inter.projectInter.dto.UserResponseDTO;
import com.inter.projectInter.entity.Digits;
import com.inter.projectInter.entity.User;
import com.inter.projectInter.repository.DigitsRepository;
import com.inter.projectInter.repository.UserRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Test
    public void shouldCreateAndReturnUserResponse() {
        UserRequestDTO newUser = buildUser();
        given(userRepository.findByEmail(Mockito.any(String.class))).willReturn(Optional.empty());
        given(userRepository.save(Mockito.any(User.class))).willReturn(new User(newUser.getName(), newUser.getEmail()));
        UserResponseDTO userResponseDTO = userService.createUser(newUser);
        Assert.assertEquals(newUser.getEmail(), userResponseDTO.getEmail());
        Assert.assertEquals(newUser.getName(), userResponseDTO.getName());
        verify(userRepository, times(1)).save(Mockito.any(User.class));
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void shoudCreateUserException() {
        UserRequestDTO newUser = buildUser();
        given(userRepository.findByEmail(Mockito.any(String.class))).willReturn(Optional.ofNullable(new User()));
        verify(userRepository, times(0)).save(Mockito.any(User.class));
        userService.createUser(newUser);
    }

    @Test
    public void shoutUpdateAndReturnUserResponse() {
        UserRequestDTO updateUser = buildUser();
        given(userRepository.findById(1L)).willReturn(Optional.ofNullable(new User("user01", "email@email.com")));
        given(userRepository.save(Mockito.any(User.class))).willReturn(new User(updateUser.getName(), updateUser.getEmail()));
        UserResponseDTO userResponse = userService.updateUser(updateUser, 1L);
        Assert.assertEquals(updateUser.getEmail(), userResponse.getEmail());
        Assert.assertEquals(updateUser.getName(), userResponse.getName());
        verify(userRepository, times(1)).save(Mockito.any(User.class));
    }

    @Test
    public void shouldFindAndReturnUserResponse() {
        UserRequestDTO userRequestDTO = buildUser();
        given(userRepository.findById(1L)).willReturn(Optional.ofNullable(new User(userRequestDTO.getName(), userRequestDTO.getEmail())));
        UserResponseDTO user = userService.findUser(1L);
        Assert.assertEquals(userRequestDTO.getEmail(), user.getEmail());
        Assert.assertEquals(userRequestDTO.getName(), user.getName());

    }

    private UserRequestDTO buildUser() {
        return new UserRequestDTO("user01", "user01@gmail.com", "123");
    }


}
