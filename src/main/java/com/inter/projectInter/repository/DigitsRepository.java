package com.inter.projectInter.repository;

import com.inter.projectInter.entity.Digits;
import com.inter.projectInter.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DigitsRepository extends JpaRepository<Digits, Long> {

    public List<Digits> findDigitsByUser(User user);
}
