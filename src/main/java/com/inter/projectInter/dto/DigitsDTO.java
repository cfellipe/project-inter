package com.inter.projectInter.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@EqualsAndHashCode(exclude={"total"})
public class DigitsDTO {

    private String value;
    private Integer multiplier;
    private Integer total;

    public DigitsDTO(){
        
    }

    public DigitsDTO(String value, Integer multiplier) {
        this.value = value;
        this.multiplier = multiplier;
    }
}
