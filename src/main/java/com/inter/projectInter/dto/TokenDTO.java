package com.inter.projectInter.dto;

import lombok.*;

@Setter
@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
public class TokenDTO {
    private String token;
    private String type;
}
