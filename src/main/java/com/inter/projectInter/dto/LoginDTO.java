package com.inter.projectInter.dto;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.validation.constraints.NotEmpty;

@Setter
@Getter
@ToString
public class LoginDTO {


    private String email;
    private String senha;
}