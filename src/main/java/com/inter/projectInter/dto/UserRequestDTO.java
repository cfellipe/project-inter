package com.inter.projectInter.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class UserRequestDTO {

    private String name;
    private String email;
    private String passwordUser;

    public UserRequestDTO(){

    }

    public UserRequestDTO(String name, String email, String passwordUser) {
        this.name = name;
        this.email = email;
        this.passwordUser = passwordUser;
    }
}
