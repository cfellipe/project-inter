package com.inter.projectInter.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "tb_digits")
public class Digits extends BaseEntity {

    private String value;
    private Integer multiplier;
    private Integer total;
    @ManyToOne
    private User user;

    public Digits(){

    }

    public Digits(String value, Integer multiplier) {
        this.value = value;
        this.multiplier = multiplier;
    }
}
