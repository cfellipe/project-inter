package com.inter.projectInter.entity;


import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Collection;
import java.util.List;


@Entity
@Getter
@Setter
@Table(name = "tb_user")
public class User extends BaseEntity implements UserDetails {

    private String name;
    private String email;
    private String passwordUser;
    @OneToMany(mappedBy = "user")
    private List<Digits> digits;
    @Column(length = 3000)
    private String publicKey;

    public User(){

    }

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return this.passwordUser;
    }

    @Override
    public String getUsername() {
        return this.name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
