package com.inter.projectInter.controller;


import com.inter.projectInter.dto.UserResponseDTO;
import com.inter.projectInter.service.CryptographyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/cryptography")
public class CryptographyController {

    @Autowired
    private CryptographyService cryptographyService;


    @ApiOperation(value = "Get public key for user")
    @PutMapping("/{userId}")
    public ResponseEntity<String> generatePublicKey(@PathVariable Long userId) {
        return ResponseEntity.status(HttpStatus.OK).body(cryptographyService.getPublicKey(userId));
    }

    @ApiOperation(value = "Encrypt user information")
    @PutMapping("/encrypt/{userId}")
    public ResponseEntity<UserResponseDTO>encripty(@PathVariable Long userId, @RequestParam String publicKey) {
        return ResponseEntity.status(HttpStatus.OK).body(cryptographyService.encryptUserInformation(userId,publicKey));
    }

}
