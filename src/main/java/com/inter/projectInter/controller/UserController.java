package com.inter.projectInter.controller;

import com.inter.projectInter.dto.LoginDTO;
import com.inter.projectInter.dto.TokenDTO;
import com.inter.projectInter.dto.UserRequestDTO;
import com.inter.projectInter.dto.UserResponseDTO;
import com.inter.projectInter.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "Create a new user")
    @PostMapping()
    public ResponseEntity<UserResponseDTO> createUser(@RequestBody @Valid UserRequestDTO userRequestDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(userRequestDTO));
    }

    @ApiOperation(value = "Find user by id")
    @GetMapping({"/{userId}"})
    public ResponseEntity<UserResponseDTO> findUserById(@PathVariable Long userId) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.findUser(userId));
    }

    @ApiOperation(value = "Delete user by id")
    @DeleteMapping("/{userId}")
    public ResponseEntity<UserRequestDTO> deleteUser(@PathVariable Long userId) {
        userService.deleteUser(userId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ApiOperation(value = "Update a user by id")
    @PutMapping("/{userId}")
    public ResponseEntity<UserResponseDTO> updateUser(@RequestBody UserRequestDTO userRequestDTO, @PathVariable Long userId) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.updateUser(userRequestDTO, userId));
    }

}
