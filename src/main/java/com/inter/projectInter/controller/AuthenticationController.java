package com.inter.projectInter.controller;

import com.inter.projectInter.dto.LoginDTO;
import com.inter.projectInter.dto.TokenDTO;
import com.inter.projectInter.service.AuthenticationService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @ApiOperation(value = "Authenticate user")
    @PostMapping()
    public ResponseEntity<TokenDTO> authenticate(@RequestBody @Valid LoginDTO form){
        return ResponseEntity.status(HttpStatus.OK).body(authenticationService.authUser(form));
    }
}
