package com.inter.projectInter.controller;

import com.inter.projectInter.dto.DigitsDTO;
import com.inter.projectInter.entity.User;
import com.inter.projectInter.service.DigitsService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/digits")
public class DigitsController {

    @Autowired
    private DigitsService digitsService;

    @ApiOperation(value = "Calculate a unique digits")
    @PostMapping()
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", example = "Bearer access_token")
    public ResponseEntity<Integer> calculateUniqueDigitis(@RequestBody DigitsDTO digitsDTO, Authentication auth) {
        return ResponseEntity.status(HttpStatus.OK).body(digitsService.calculate(digitsDTO, (User) auth.getPrincipal()));
    }

    @ApiOperation(value = "Get digits by user")
    @GetMapping("/user")
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", example = "Bearer access_token")
    public ResponseEntity<List<DigitsDTO>> getDigitsbyUser(Authentication auth) {
        return ResponseEntity.status(HttpStatus.OK).body(digitsService.getDigitsByUser((User) auth.getPrincipal()));
    }
}
