package com.inter.projectInter.security;

import com.inter.projectInter.entity.User;
import com.inter.projectInter.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TokenService implements UserDetailsService {

    @Value("${config.jwt.expiration}")
    private String expiration;

    @Value("${config.jwt.secret}")
    private String secret;

    @Autowired
    private UserRepository usuarioRepository;

    public String generateToken(Authentication authentication) {
        User logado = (User) authentication.getPrincipal();
        return Jwts.builder()
                .setIssuer("project Inter")
                .setSubject(logado.getId().toString())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + Long.parseLong(this.expiration)))
                .signWith(io.jsonwebtoken.SignatureAlgorithm.HS256, secret)
                .compact();
    }

    public boolean isValidToken(String token) {
        try {
            Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token);
            return true;
        }catch (Exception e){
            return false;
        }

    }

    public Long getIdUser(String token) {
        Claims claims = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token).getBody();
        return Long.parseLong(claims.getSubject());
    }


    @Override
    public UserDetails loadUserByUsername(String userEmail) throws UsernameNotFoundException {
        return usuarioRepository.findByEmail(userEmail).orElseThrow(()-> new UsernameNotFoundException("Dados inválidos"));
    }
}
