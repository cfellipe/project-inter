package com.inter.projectInter.service;

import com.inter.projectInter.config.exception.DataIntegrityViolationException;
import com.inter.projectInter.config.exception.ObjectNotFoundException;
import com.inter.projectInter.dto.UserRequestDTO;
import com.inter.projectInter.dto.UserResponseDTO;
import com.inter.projectInter.entity.User;
import com.inter.projectInter.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private UserRepository userRepository;

    public UserResponseDTO createUser(UserRequestDTO userRequestDto) {
        validateExistsUser(userRequestDto);
        User user = modelMapper.map(userRequestDto, User.class);
        user.setPasswordUser(encriptPassword(userRequestDto.getPasswordUser()));
        User savedUser = userRepository.save(user);
        return modelMapper.map(savedUser, UserResponseDTO.class);
    }

    private void validateExistsUser(UserRequestDTO userRequestDto) {
        if (userRepository.findByEmail(userRequestDto.getEmail()).isPresent()) {
            throw new DataIntegrityViolationException("Already exists a user account for this email " + userRequestDto.getEmail() + ", type: " + User.class.getName());
        }
    }

    public UserResponseDTO findUser(Long userId) {
        User user = findUserById(userId);
        return modelMapper.map(user, UserResponseDTO.class);
    }

    public UserResponseDTO updateUser(UserRequestDTO userRequestDTO, Long id) {
        User user = findUserById(id);
        convertUserToUpdate(userRequestDTO, user);
        user = userRepository.save(user);
        return modelMapper.map(user, UserResponseDTO.class);
    }

    public void deleteUser(Long id) {
        User user = findUserById(id);
        userRepository.delete(user);
    }

    public  User findUserById(Long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new ObjectNotFoundException("Object not found! Id: " + userId + ", type: " + User.class.getName()));
    }

    private String encriptPassword(String password) {
        return new BCryptPasswordEncoder().encode(password);
    }

    private User convertUserToUpdate(UserRequestDTO userRequestDTO, User user) {
        if (StringUtils.isNotBlank(userRequestDTO.getName())) {
            user.setName(userRequestDTO.getName());
        }
        if (StringUtils.isNotBlank(userRequestDTO.getEmail())) {
            user.setEmail(userRequestDTO.getEmail());
        }
        return user;
    }
}
