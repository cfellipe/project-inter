package com.inter.projectInter.service;

import com.inter.projectInter.dto.UserRequestDTO;
import com.inter.projectInter.dto.UserResponseDTO;
import com.inter.projectInter.entity.User;
import com.inter.projectInter.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import java.io.*;
import java.security.*;
import java.security.interfaces.ECPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;

@Service
public class CryptographyService {

    public static final String ALGORITHM = "RSA";
    public static final String PATH_CHAVE_PRIVADA = ".keys/private";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;


    public String getPublicKey(Long userId) {

        User user = userService.findUserById(userId);
        if (!userHasPublicKey(user)) {
            Optional.ofNullable(generateKeys(userId)).ifPresent(c -> {
                user.setPublicKey(convertToString(c.getPublic()));
            });
            User save = userRepository.save(user);
        }
        return user.getPublicKey();

    }

    public UserResponseDTO encryptUserInformation(Long id, String publicKey) {
        User user = userService.findUserById(id);
        PublicKey key = convertToPublicKey(publicKey);
        user.setEmail(encript(user.getEmail(), key));
        user.setName(encript(user.getName(), key));
        User savedUser = userRepository.save(user);
        return modelMapper.map(savedUser, UserResponseDTO.class);
    }

    //decrypt method doesn't work
    public UserResponseDTO decryptUserInformation(Long id) {
        User user = userService.findUserById(id);
        user.setEmail(decript(user.getEmail().getBytes(), id));
        user.setName(decript(user.getName().getBytes(), id));
        User savedUser = userRepository.save(user);
        return modelMapper.map(savedUser, UserResponseDTO.class);
    }

    private KeyPair generateKeys(Long userId) {
        try {
            final KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM);
            keyGen.initialize(2048);
            final KeyPair key = keyGen.generateKeyPair();
            File chavePrivadaFile = new File(PATH_CHAVE_PRIVADA + userId.toString() + ".key");
            if (chavePrivadaFile.getParentFile() != null) {
                chavePrivadaFile.getParentFile().mkdirs();
            }
            chavePrivadaFile.createNewFile();
            ObjectOutputStream chavePrivadaOS = new ObjectOutputStream(new FileOutputStream(chavePrivadaFile));
            chavePrivadaOS.writeObject(key.getPrivate());
            chavePrivadaOS.close();
            return key;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String encript(String texto, PublicKey chave) {
        byte[] cipherText = null;
        try {
            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, chave);
            cipherText = cipher.doFinal(texto.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cipherText.toString();
    }

    private String decript(byte[] text, Long userId) {
        byte[] dectyptedText = null;

        try {
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(PATH_CHAVE_PRIVADA + userId.toString() + ".key"));
            final PrivateKey privateKey = (PrivateKey) inputStream.readObject();
            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            dectyptedText = cipher.doFinal(text);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new String(dectyptedText);
    }

    private boolean userHasPublicKey(User user) {
        return Optional.ofNullable(user.getPublicKey()).isPresent();
    }

    private String convertToString(PublicKey publicKey) {
        byte[] byte_pubkey = publicKey.getEncoded();
        return Base64.getEncoder().encodeToString(byte_pubkey);
    }

    private PublicKey convertToPublicKey(String publicKey) {
        byte[] byte_pubkey = Base64.getDecoder().decode(publicKey);
        KeyFactory factory = null;
        try {
            factory = KeyFactory.getInstance(ALGORITHM);
            return factory.generatePublic(new X509EncodedKeySpec(byte_pubkey));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }
}
