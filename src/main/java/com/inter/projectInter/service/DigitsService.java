package com.inter.projectInter.service;

import com.inter.projectInter.dto.DigitsDTO;
import com.inter.projectInter.entity.Digits;
import com.inter.projectInter.entity.User;
import com.inter.projectInter.repository.DigitsRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class DigitsService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DigitsRepository digitsRepository;

    private List<DigitsDTO> cacheDigitis = new ArrayList<>(10);
    private int count = 0;

    public List<DigitsDTO> getDigitsByUser(User user){
        List<Digits> digitsByUser = digitsRepository.findDigitsByUser(user);
        List<DigitsDTO> dto = digitsByUser.stream().map(e -> modelMapper.map(e, DigitsDTO.class)).collect(Collectors.toList());
        return dto;
    }

    public Integer calculate(DigitsDTO digitsDTO, User user) {

        Optional<DigitsDTO> optDigits = searchCache(digitsDTO);

        if (optDigits.isPresent()) {
            return optDigits.get().getTotal();
        } else {
            int total = uniqueDigit(digitsDTO.getValue(), digitsDTO.getMultiplier());
            digitsDTO.setTotal(total);
            addCacheDigits(digitsDTO);
            Digits digits = modelMapper.map(digitsDTO, Digits.class);
            digits.setUser(user);
            digitsRepository.save(digits);
            return total;
        }

    }

    public List<DigitsDTO> getCacheDigitis(){
        return cacheDigitis;
    }

    public void setCacheDigitis(List<DigitsDTO> cacheDigitis) {
        this.cacheDigitis = cacheDigitis;
    }

    private Optional<DigitsDTO> searchCache(DigitsDTO digitsDTO) {
        return cacheDigitis.stream().filter(c -> c.equals(digitsDTO)).findFirst();
    }

    private int uniqueDigit(String number, Integer multiplier) {
        StringBuilder stbValue = new StringBuilder();
        if (multiplier != null && multiplier > 0) {
            IntStream.range(0, multiplier).forEach(i -> stbValue.append(number));
        } else {
            stbValue.append(number);
        }
        return Arrays.stream(stbValue.toString().split("")).map(Integer::valueOf).mapToInt(Integer::intValue).sum();
    }

    private void addCacheDigits(DigitsDTO digits) {
        if (count == 10) {
            count = 0;
        }
        if (cacheDigitis.size() == 10) {
            cacheDigitis.remove(count);
            cacheDigitis.add(count, digits);
            count++;
        } else {
            cacheDigitis.add(digits);
        }
    }

}
