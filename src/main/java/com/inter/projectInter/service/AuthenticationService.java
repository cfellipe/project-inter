package com.inter.projectInter.service;

import com.inter.projectInter.config.exception.ObjectNotFoundException;
import com.inter.projectInter.dto.LoginDTO;
import com.inter.projectInter.dto.TokenDTO;
import com.inter.projectInter.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TokenService tokenService;

    public TokenDTO authUser(LoginDTO form) {
        UsernamePasswordAuthenticationToken dadosLogin = new UsernamePasswordAuthenticationToken(form.getEmail(),form.getSenha());
        try {
            Authentication authentication = authenticationManager.authenticate(dadosLogin);
            String token = tokenService.generateToken(authentication);
            return new TokenDTO(token,"Bearer");
        }catch (AuthenticationException e){
            throw new ObjectNotFoundException("Incorrect password");
        }
    }
}
